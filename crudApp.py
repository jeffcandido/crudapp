from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtWidgets import QDialog, QInputDialog, QLineEdit, QMessageBox
from PyQt5.QtGui import QIcon
import MySQLdb as mdb

class Ui_Dialog(QDialog):
    def setupUi(self, Dialog):
        Dialog.setObjectName("Dialog")
        Dialog.resize(368, 278)
        self.widget = QtWidgets.QWidget(Dialog)
        self.widget.setGeometry(QtCore.QRect(10, 20, 346, 247))
        self.widget.setObjectName("widget")
        self.horizontalLayout = QtWidgets.QHBoxLayout(self.widget)
        self.horizontalLayout.setContentsMargins(11, 11, 11, 11)
        self.horizontalLayout.setSpacing(6)
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.listWidget = QtWidgets.QListWidget(self.widget)
        self.listWidget.setObjectName("listWidget")
        self.horizontalLayout.addWidget(self.listWidget)
        self.verticalLayout = QtWidgets.QVBoxLayout()
        self.verticalLayout.setSpacing(6)
        self.verticalLayout.setObjectName("verticalLayout")
        self.pushButton_add = QtWidgets.QPushButton(self.widget)
        self.pushButton_add.setObjectName("pushButton_add")
        self.verticalLayout.addWidget(self.pushButton_add)
        self.pushButton_edit = QtWidgets.QPushButton(self.widget)
        self.pushButton_edit.setObjectName("pushButton_edit")
        self.verticalLayout.addWidget(self.pushButton_edit)
        self.pushButton_remove = QtWidgets.QPushButton(self.widget)
        self.pushButton_remove.setObjectName("pushButton_remove")
        self.verticalLayout.addWidget(self.pushButton_remove)
        self.pushButton_up = QtWidgets.QPushButton(self.widget)
        self.pushButton_up.setObjectName("pushButton_up")
        self.verticalLayout.addWidget(self.pushButton_up)
        self.pushButton_down = QtWidgets.QPushButton(self.widget)
        self.pushButton_down.setObjectName("pushButton_down")
        self.verticalLayout.addWidget(self.pushButton_down)
        self.pushButton_sort = QtWidgets.QPushButton(self.widget)
        self.pushButton_sort.setObjectName("pushButton_sort")
        self.verticalLayout.addWidget(self.pushButton_sort)
        spacerItem = QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        self.verticalLayout.addItem(spacerItem)
        self.pushButton_close = QtWidgets.QPushButton(self.widget)
        self.pushButton_close.setObjectName("pushButton_close")
        self.verticalLayout.addWidget(self.pushButton_close)
        self.horizontalLayout.addLayout(self.verticalLayout)
# jEFF
        self.pushButton_add.clicked.connect(self.Add)
        self.pushButton_edit.clicked.connect(self.Edit)
        self.pushButton_remove.clicked.connect(self.Remove)
        self.pushButton_up.clicked.connect(self.Up)
        self.pushButton_down.clicked.connect(self.Down)
        self.pushButton_sort.clicked.connect(self.Sort)
        self.pushButton_close.clicked.connect(self.Close)
        

        self.retranslateUi(Dialog)
        QtCore.QMetaObject.connectSlotsByName(Dialog)
# jEFF
        self.Employee()
        self.DBConnection()

    def retranslateUi(self, Dialog):
        _translate = QtCore.QCoreApplication.translate
        Dialog.setWindowTitle(_translate("Dialog", "CRUD App"))
        Dialog.setWindowIcon(QIcon("icon.png"))
        self.pushButton_add.setText(_translate("Dialog", "Add"))
        self.pushButton_edit.setText(_translate("Dialog", "Edit"))
        self.pushButton_remove.setText(_translate("Dialog", "Remove"))
        self.pushButton_up.setText(_translate("Dialog", "Up"))
        self.pushButton_down.setText(_translate("Dialog", "Down"))
        self.pushButton_sort.setText(_translate("Dialog", "Sort"))
        self.pushButton_close.setText(_translate("Dialog", "Close"))

# jEFF
    def Employee(self):

        self.employee = []

        con = mdb.connect('localhost', 'root', 'password', 'pyqt5')
        with con:
            cur = con.cursor()

            cur.execute("SELECT * FROM data")

            for linha in cur.fetchall():
                # print(linha)
                self.employee.append(linha[1])

        self.listWidget.addItems(self.employee)
        self.listWidget.setCurrentRow(0)
# jEFF
    def Add(self):        
        row = self.listWidget.currentRow()
        text, ok = QInputDialog.getText(self, "Estagiário", "Entre com o nome")

        if ok is not None and text != '':
            self.InsertData(text)
            self.listWidget.insertItem(row, text)

# jEFF
    def Edit(self):
        row = self.listWidget.currentRow()
        item = self.listWidget.item(row)

        if item is not None:
            string, ok = QInputDialog.getText(self, "Employee Dialog", "Edit Employee Name", QLineEdit.Normal, item.text())
            if ok and string is not None:
                # print(item.text(), string)
                self.EditData(item.text(), string)
                item.setText(string)

# jEFF
    def Remove(self):
        row = self.listWidget.currentRow()
        item = self.listWidget.item(row)

        if item is None:
            return

        reply = QMessageBox.question(self, "Remover Estagiario", "Voce quer mesmo remover o estagiario " + str(item.text()) + "?",
                                     QMessageBox.Yes | QMessageBox.No)

        if reply == QMessageBox.Yes:
            item = self.listWidget.takeItem(row)
            self.RemoveData(item.text())
            del item

# jEFF
    def Up(self):
        row = self.listWidget.currentRow()

        if row >= 1:
            item = self.listWidget.takeItem(row)
            self.listWidget.insertItem(row - 1, item)
            self.listWidget.setCurrentItem(item)

# jEFF
    def Down(self):
        row = self.listWidget.currentRow()

        if row < self.listWidget.count() - 1 :
            item = self.listWidget.takeItem(row)
            self.listWidget.insertItem(row + 1, item)
            self.listWidget.setCurrentItem(item)

# jEFF
    def Sort(self):
        self.listWidget.sortItems()

# jEFF
    def Close(self):
        quit()

# Conexão com o Banco de dados
    def DBConnection(self):
        try:
            db = mdb.connect('localhost', 'root', 'password', 'pyqt5')
            QMessageBox.about(self, 'Conexão', 'Conectado ao banco de dados corretamente!')

        except mdb.Error as e:
            QMessageBox.about(self, 'Conexão', 'Falha ao se conectar ao Banco de Dados')
            sys.exit(1)

# Inserção de dados no banco

    def InsertData(self, name):
        con = mdb.connect('localhost', 'root', 'password', 'pyqt5')
        with con:
            cur = con.cursor()

            cur.execute("INSERT INTO data(name)"
                        "VALUES('%s')" % name)

            con.commit()

            QMessageBox.about(self,'Conexão', 'Estagiário Inserido com sucesso!')
            self.close()

# Edição de dados no banco

    def EditData(self, old_name, new_name):
        con = mdb.connect('localhost', 'root', 'password', 'pyqt5')
        with con:
            cur = con.cursor()

            # print(old_name, new_name)

            sql = "UPDATE data SET name = %s WHERE name = %s"
            val = (new_name, old_name )

            cur.execute( sql, val )

            con.commit()

            QMessageBox.about(self,'Conexão', 'Nome editado com sucesso!')
            self.close()

# Remoção de dados no banco

    def RemoveData(self, name):
        con = mdb.connect('localhost', 'root', 'password', 'pyqt5')
        with con:
            cur = con.cursor()

            sql = "DELETE FROM data WHERE name = %s"

            adr = (name, )

            # print(name)

            cur.execute( sql , adr )

            con.commit()

            QMessageBox.about(self,'Conexão', 'Estagiário Removido com sucesso!')
            self.close()


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    Dialog = QtWidgets.QDialog()
    ui = Ui_Dialog()
    ui.setupUi(Dialog)
    Dialog.show()
    sys.exit(app.exec_())
